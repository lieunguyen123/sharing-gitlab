# I. Install git    
https://phoenixnap.com/kb/how-to-install-git-windows

# II. Repository
1. Create new project
    ```
    $ mkdir create-new-repo
    $ cd create-new-repo/
    $ git init
    $ vi README.md
    $ git add.
    $ git commit -m "message"
    $ git remote add origin  https://gitlab.com/lieunguyen123/new-repo.git
    $ git push origin master
    ```
2. Delete project
    ```
    - Navigate to your project, and select Settings > General > Advanced.
    - In the “Delete project” section, click the Delete project button.
    - Confirm the action when asked to.
    ```
3. Clone the existed project
    ```
    $ git clone https://gitlab.com/lieunguyen123/sharing-gitlab.git
    ```

# III. Remote
1. Check the existed remote
    ```
    $ git remote -v
    ```
2. Add new remote
    ```
    $ git remote add new_remote https://your_name@gitlab.com/lieunguyen123/create-new-repo.git 
    ```
3. Update remote name
    ```
    $ git remote rename old_name new_name
    ```
4. Delete remote
    ```
    $ git remote remove remote_name
    ```
# IV. Branch
1. Check the existed Branch
    ```
    $ git branch
    ```
2. Create Branch
    ```
    # Branch doesn't exist on gitlab
    $ git checkout -b new_branch 

    # Existed branch on gitlab
    $ git fetch
    $ git checkout -b other_branch_local other_branch_server
    ```
3. Update branch name
    ```
    $ git branch -m new_name (current branch)
    $ git branch -m old_branch new_branch (other branch)
    ```
4. Checkout other branch
    ```
    $ git checkout other_branch_server
    ```
5. Check status in branch
    ```
    $ git status
    ```
6. Delete branch
    ```
    $ git branch -D branch_name (Delete branch on local)
    $ git push remote_name --delete branch_name (Delete branch on gitlab)
    ```
7. Push new_branch to gitlab
    ```
    $ git push remote_name new_branch
    ```
# V. Commit
1. Push commit
    ```
    $ git add file_name
    $ git commit -m "message issue #1"
    $ git push
    ```
2. Pull commit
    ```
    $ git pull
    ```
3. Revert commit
    ```
    $ git reset HEAD~1 (on local)
    $ git push remote_name +commit_id:branch_name (on gitlab)
    ```
4. Check the changed code
    ```
    $ git diff
    ```
5. Remove the changed code
    ```
    $ git reset --hard (remove the changed all)
    $ git checkout -- file_name (remove the changed all in file)
    ```
6. Merged commit from other branch
    ```
    $ git pull origin other_branch
    ```
7. Resolve conflict
    ```
    update code in the conflicted file -> commit to gitlab
    ```
# VI. Gitlab UI
1. Issue
- Create new issue
- Comment on issue
- Close issue
2. Merge request
- Create Merge request
- Approve
- Merge code
- Close merge request

